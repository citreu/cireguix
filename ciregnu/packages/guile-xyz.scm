(define-module (ciregnu packages guile-xyz)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)

  #:use-module (guix build-system guile)

  #:use-module (gnu packages guile)
  #:use-module (gnu packages texinfo))

(define-public guile-msgpack
  (let ((commit "6a997c7dd55f587128a0a5ebe2daee2f206d48e3")
        (revision "0"))
    (package
      (name "guile-msgpack")
      (version (git-version "0.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/HiPhish/guile-msgpack")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1kyl61l89qw0agb85hjk9kw3d1375gcy5y3b68w93xb9a3w29y32"))))
      (build-system guile-build-system)
      (arguments
       `(#:modules ((guix build guile-build-system)
                    (guix build utils)
                    (ice-9 ftw)
                    (ice-9 regex)
                    (srfi srfi-26))
         #:phases
         (modify-phases %standard-phases
           ;; TODO: Enable test
           (add-before 'build 'remove-test
             (lambda _
               (delete-file-recursively "test")
               #t))
           (add-after 'build 'install-info
             (lambda* (#:key outputs  #:allow-other-keys)
               (with-directory-excursion "doc"
                 (let* ((texis (scandir "." (cut string-match "\\.texi$" <>)))
                        (out (assoc-ref outputs "out"))
                        (dir (string-append out "/share/info")))
                   (for-each
                    (lambda (file)
                      (format #t "instal-info: Installing ~a as info~%" file)
                      (invoke "texi2any" "--info" "-I" "./include"
                              (format #f "--output=~a/~a.info"
                                      dir (basename file ".texi"))
                              file))
                    texis)))
               #t)))))
      (native-inputs
       `(("texinfo" ,texinfo)
         ("guile" ,guile-3.0)))
      (home-page "https://gitlab.com/HiPhish/guile-msgpack")
      (synopsis "MessagePack for GNU Guile")
      (description "Guile-msgpack implments the MessagePack data serialisation
format. It allows you to serialize and de-serialize Scheme object to and from
binary data.")
      (license license:gpl3+))))
