(define-module (ciregnu packages xremap)
  #:use-module (gnu packages)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module ((guix licenses) #:prefix license:))

(define-public xremap-bin
  (package
    (name "xremap-bin")
    (version "0.7.8")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri (string-append "https://github.com/k0kubun/xremap/releases/download/v"
                           version "/xremap-linux-x86_64-x11.zip"))
       (sha256
        (base32 "1rm7lfgsdvbw540vbiwp5kqciq36ifmzlw380vpgdsbwq98fjjpi"))))
    (build-system copy-build-system)
    (arguments
     (list
      #:install-plan #~'(("xremap" "bin/xremap"))))
    (supported-systems '("x86_64-linux"))
    (home-page "https://github.com/k0kubun/xremap")
    (synopsis "Binary distribution of xremap")
    (description "Xremap is a key remapper for Linux.")
    (license license:expat)))
