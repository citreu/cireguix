(define-module (ciregnu packages networking)
  #:use-module (guix base16)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system trivial)
  #:use-module ((guix licenses) #:prefix license:))

(define-public hysteria-bin
  (package
    [name "hysteria-bin"]
    [version "1.3.4"]
    [source
     (origin
       [method url-fetch]
       [uri
        (string-append "https://github.com/apernet/hysteria/releases/download/v"
                       version "/hysteria-linux-amd64")]
       [file-name (string-append name "-" version)]
       [sha256
        (base16-string->bytevector
         "d2298ad5a1d70cf52f90c5572053ff901b725e8ddbb58742ee495f7d04724c0c")])]
    [build-system trivial-build-system]
    [arguments
     (list
      #:modules '((guix build utils))
      #:builder
      #~(begin
          (use-modules (guix build utils))
          (let ((dest (string-append #$output "/bin/hysteria")))
            (mkdir-p (dirname dest))
            (copy-file #$(package-source this-package) dest)
            (chmod dest #o555))))]
    [supported-systems '("x86_64-linux")]
    [home-page "https://hysteria.network"]
    [synopsis "QUIC protocol based proxy tool"]
    [description
     "Hysteria is proxy and relay tool optimized for lossy or unstable
connection.  It's powered by a customized QUIC protocol."]
    [license license:expat]))

(define-public sing-box-bin
  (package
    [name "sing-box-bin"]
    [version "1.2.2"]
    [source
     (origin
       [method url-fetch]
       [uri (string-append
             "https://github.com/SagerNet/sing-box/releases/download/v"
             version "/sing-box-" version "-linux-amd64.tar.gz")]
       [sha256
        (base32
         "0k3lmkyqhc9xryll95rz5kyyp9kg1qaqjgarnjmvic42rx9lqy1y")])]
    [build-system copy-build-system]
    [arguments
     (list
      #:install-plan
      #~'(("sing-box" "bin/")))]
    [supported-systems '("x86_64-linux")]
    [home-page "https://sing-box.sagernet.org"]
    [synopsis "Universal proxy platform"]
    [description
     "Sing-box is a universal proxy platform supports many difference proxy
protocol to bypass the network censorship."]
    [license license:gpl3+]))

(define-public xray-bin
  (package
    [name "xray-bin"]
    [version "1.8.8"]
    [source
     (origin
       [method url-fetch/zipbomb]
       [uri (string-append "https://github.com/XTLS/Xray-core/releases/download/v"
                           version "/Xray-linux-64.zip")]
       [file-name (string-append "xray-bin-" version ".zip")]
       [sha256
        (base16-string->bytevector
         "c262dfedf1f85487b130d4f598201ab0a45b4540a25a074d30b6559e50a1a741")])]
    [build-system copy-build-system]
    [arguments
     (list
      #:install-plan
      #~'(("xray" "bin/")
          ("." "share/xray-geodata" #:include ("dat")))
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'install 'wrap-geodata
            (lambda _
              (let* ((bin (string-append #$output "/bin"))
                     (assets-dir (string-append #$output
                                                "/share/xray-geodata")))
                (wrap-program (string-append bin "/xray")
                  `("XRAY_LOCATION_ASSET" = (,assets-dir)))
                #t)))))]
    [supported-systems '("x86_64-linux")]
    [home-page "https://github.com/XTLS/Xray-core"]
    [synopsis "Binary version of XRay"]
    [description
     "Xray-bin is platform for building proxies to bypass network restrictions."]
    [license license:mpl2.0]))
