(define-module (ciregnu services nix-ld)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages bash)
  #:use-module (ciregnu packages elf)
  #:use-module (gnu services base)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services web)
  #:use-module (gnu services)
  #:use-module (gnu system shadow)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (guix store)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (guix modules)
  #:export (nix-ld-service-type

            nix-ld-configuration
            nix-ld-configuration?))

(define-record-type* <nix-ld-configuration>
  nix-ld-configuration make-nix-ld-configuration
  nix-ld-configuration?
  (package             nix-ld-configuration-package ;file-like
                       (default nix-ld))
  (linker              nix-ld-configuration-linker))

(define nix-ld-activation
  (match-lambda
    (($ <nix-ld-configuration> package linker)
     #~(begin
         (use-modules (guix build utils))

         (define (switch-symlinks link target)
           (let ((pivot (string-append link ".new")))
             (symlink target pivot)
             (rename-file pivot link)))

         (mkdir-p #$(dirname linker))
         (switch-symlinks
          #$linker
          (string-append #$package "/libexec/nix-ld"))))))

(define nix-ld-service-type
  (service-type
   (name 'nix-ld)
   (extensions
    (list (service-extension activation-service-type nix-ld-activation)))
   (description "Enable Nix-ld linker.")))
